/**
 * Travail Pratique dans le cadre du cours Programmation 2 - INF2120.
 * Traduction de formes romanisées des Hanguls en table HTML.
 *
 * @author (Maxime Masson)
 * @codepermanent (MASM06079507)
 * @author (Alexis Bouchard)
 * @codepermanent ()
 */

import java.util.*;
import java.io.*;

class Principal {

    public static final String DEBUT_HTML = "<!DOCTYPE HTML>\n" +
            "  <HTML>\n" +
            "    <head>\n" +
            "      <title>Tp2</title>\n" +
            "    </head>\n" +
            "  <body>\n" +
            "    <hr>\n" +
            "    <table>";
    public static final String FIN_HTML = "    </table>\n" +
            "    <hr>\n" +
            "  </body>\n" +
            "</HTML>";
    public static final String CHOIX_FICHIER = "Veuillez ecrire le nom" +
            " du fichier contenant la romanisation : ";



    public static void imprimerHTML (tableauHorizontal tableau) throws IOException {
        FileWriter fileWriter = new FileWriter("resultat.html");
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.println(DEBUT_HTML);
        for (int i = 0 ; i < tableau.size() ; i++) {
            printWriter.println("      <TR>" + tableau.get(i) + " </TR>");
        }
        printWriter.println(FIN_HTML);
        printWriter.close();

    }

    public static String choisirFichier () {
        System.out.print(CHOIX_FICHIER);
        Scanner sc = new Scanner( System.in );
        String nomFichierEntrees = sc.nextLine();
        return nomFichierEntrees;
    }

    public static void main(String[] params) {

        String nomFichierEntrees = choisirFichier();
        File file = new File(nomFichierEntrees);
        tableauHorizontal tableau = new tableauHorizontal();
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNextLine()) {

                String hangulTxt = sc.nextLine();
                String[] substring = hangulTxt.split("\\s+");
                Ligne ligne = new Ligne();
                for (String sub : substring) { 
                    ligne.add(new Hangul(sub));
                }
                tableau.add(ligne);
            }
            tableau.remplir(tableau.longueurMax());
            tableau.rendreVertical();
            try {
                imprimerHTML(tableau);
            } catch (IOException e) {
                System.out.println("Erreur d'imprimerie");
            }

            for (int i = 0 ; i < tableau.size() ; i++) {
            System.out.println("      <TR>" + tableau.get(i) + " </TR>");
        }
            sc.close();
        } catch (FileNotFoundException e) {
            System.out.println("Veuillez entrer un fichier valide en argument");
        }
    }
}