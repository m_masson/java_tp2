import java.util.ArrayList;

/**
 * Classe décrivant une ligne de Hanguls.
 * 
 * 
 */

public class Ligne extends ArrayList<Hangul> {

    /**
     * Constructeur ne prenant aucun paramètre. Tient de sa
     * super classe.
     */
    public Ligne() {
        super();
    }

    /**
     * Definit une representation sous forme de chaine de caracteres
     * de cet objet.
     * 
     * @return Chaque element de la <code>Ligne</code>
     */
    public String toString () {
        String ligne = "";
        for (int i = 0; i < this.size(); i++) {
            ligne = ligne + " " + this.get(i);
        }
        return ligne;
    }
}