import java.util.*;

/**
 * Classe décrivant un Hangul
 * 
 */
public class Hangul {
    private String hangul;
    private List<String> consonneInitiale = List.of("G", "GG", "N", "D", "DD", "R", "M", "B", "BB", "S", "SS", "null",
            "J", "JJ", "C", "K", "T", "P", "H");
    private List<String> voyelle = List.of("A", "AE", "YA", "YAE", "EO", "E", "YEO", "YE", "O", "WA", "WAE", "OE", "YO",
            "U", "WEO", "WE", "WI", "YU", "EU", "YI", "I");
    private List<String> consonneFinale = List.of("null", "G", "GG", "GS", "N", "NJ", "NH", "D", "L", "LG", "LM", "LB",
            "LS", "LT", "LP", "LH", "M", "B", "BS", "S", "SS", "NG", "J", "C", "K", "T", "P", "H");
    private List<Character> hangulVoyelle = List.of('A', 'E', 'I', 'O', 'U', 'Y', 'W');
    private int ci;
    private int v;
    private int cf;

    /**
     * Constructeur.
     *  
     * @param hangul Un string en format Romanisé
     */
    public Hangul(String hangul) {
        this.hangul = hangul;
    }

    /**
     * Calcul la consonne initial de cet objet
     * 
     * @return Le numéro de consonne initiale de cet objet selon
     *         le tableau de Romanisation
     */
    public int calculerCi() {
        int i = 0;
        while (i < hangul.length() && (!hangulVoyelle.contains(hangul.toUpperCase().charAt(i)))) {
            i++;
        }
        if (i < hangul.length()) {
            if (i == 0) {
                ci = consonneInitiale.indexOf("null") + 1;
            } else {
                String substr = hangul.substring(0, i);
                ci = consonneInitiale.indexOf(substr.toUpperCase()) + 1;
            }
        }
        return ci;
    }
    /**
     * Calcul la consonne finale de cet objet
     * 
     * @return Le numéro de consonne finale de cet objet 
     *         selon le tableau de Romanisation
     */
    public int calculerCf() {
        int i = hangul.length() - 1;
        while (i >= 0 && (!hangulVoyelle.contains(hangul.toUpperCase().charAt(i)))) {
            i--;
        }
        if (i == hangul.length() - 1) {
            cf = consonneFinale.indexOf("null");
            ;
        } else {
            String substr = hangul.substring(i + 1, hangul.length());
            cf = consonneFinale.indexOf(substr.toUpperCase());
        }
        return cf;
    }

    /**
     * Calcul la voyelle de cet objet
     * 
     * @return Le numéro de voyelle cet objet selon le tableau de Romanisation
     */
    public int calculerV() {
        int debut = 0;
        int fin = hangul.length() - 1;
        while (debut < hangul.length() && (!hangulVoyelle.contains(hangul.toUpperCase().charAt(debut)))) {
            debut++;
        }
        while (fin >= 0 && (!hangulVoyelle.contains(hangul.toUpperCase().charAt(fin)))) {
            fin--;
        }
        String substr = hangul.substring(debut, fin + 1);
        v = voyelle.indexOf(substr.toUpperCase()) + 1;
        return v;
    }

    /**
     * Calcul la valeur unicode de cet objet
     * 
     * @param jamos1 Le numéro de consonne initiale <code>this.calculerCi</code>
     * @param jamos2 Le numéro de voyelle <code>this.calculerV</code>
     * @param jamos2 Le numéro de consonne finale <code>this.calculerCf</code>
     * @return La valeur unicode cet objet
     */
    public static int calculerUnicode(int jamos1, int jamos2, int jamos3) {
        return 44032 + 588 * (jamos1 - 1) + 28 * (jamos2 - 1) + jamos3;
    }

    /**
     * Définit une représentation sous forme de chaine de caractères de cet objet.
     * 
     * @return La valeur unicode de cet objet entouré de 
     *         balise html <code><TD/></code>.
     */
    public String toString() {
        String hangulRetourne;
        if (hangul.isEmpty()) {
            hangulRetourne = "<td width=\"15\"/>";
        } else {
            hangulRetourne = "<TD>&#" + (calculerUnicode(this.calculerCi(), this.calculerV(), this.calculerCf())) + ";</TD>";
        }
        return hangulRetourne;
    }
}
