import java.util.ArrayList;

public class tableauHorizontal extends ArrayList<Ligne> {


    public int longueurMax() {
        int plusLongue = 0;
        for (int i = 0; i < this.size(); i++) {
            if (this.get(i).size() > plusLongue) {
                plusLongue = this.get(i).size();
            }
        }
        return plusLongue;
    }

    public void remplir(int longueur) {
        for (int i = 0; i < this.size(); i++) {
            while (this.get(i).size() < longueur) {
                this.get(i).add(new Hangul(""));
            }
        }
    }

    public void rendreVertical() {
        tableauHorizontal temp = new tableauHorizontal();
        Ligne ligneAjoutee;
        for (int i = this.size() - 1; i >= 0; i--) {
            temp.add(this.get(i));
        }
        this.clear();
        for (int i = 0; i < temp.longueurMax(); i++) {
            ligneAjoutee = new Ligne();
            for (int j = 0; j < temp.size(); j++) {

                ligneAjoutee.add(temp.get(j).get(i));
            }
            this.add(ligneAjoutee);
        }
    }

}
